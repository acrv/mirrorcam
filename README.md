# README #

The MirrorCam repository is a work in progress towards making the design, code, and implementation details of the mirror-based light field video camera adapter open source.

### What is this repository for? ###

* This repo currently contains the tech report and the pdf/stl files of the current MirrorCam design
* Version: 0.5

### Contribution guidelines ###

### Who do I talk to? ###

* Contact details: 
* Dorian Tsai - dorian.tsai@gmail.com
* PhD Researcher
* Queensland University of Technology
* Australian Centre for Robotic Vision